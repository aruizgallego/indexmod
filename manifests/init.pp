class indexmod {

	file { '/web/index.php':
		ensure => file,
		mode => 600,
		owner => "root",
		group => "root",
		replace => true,
		source => "puppet:///modules/indexmod/index.php",
	}
	
	file { '/web/info.php':
		ensure => file,
		mode => 600,
		owner => "root",
		group => "root",
		replace => true,
		source => "puppet:///modules/indexmod/info.php",
	}

}

